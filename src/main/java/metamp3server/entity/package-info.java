/**
 * <p>
 * Modela las entidades de la aplicación.
 * </p>
 *
 *
 * @author MetaMP3ASAI1415
 * @since 0.1-ALPHA
 * @version 0.1-ALPHA
 */
package metamp3server.entity;