package metamp3server.entity;

import java.sql.Timestamp;
import java.util.HashMap;
import metamp3server.controller.ConstantsJSON;

/**
 * Modela una 'Pista' de la aplicación MetaMP3 para la parte del Servidor.
 *
 * @author MetaMP3ASAI1415
 * @since 0.1-ALPHA
 * @version 0.1-ALPHA
 */
public final class Pista {

    /**
     * Identificador único de la pista.
     */
    private int id;
    /**
     * Nombre de la canción de la pista.
     */
    private String nombreCancion;
    /**
     * Nombre del artista de la canción de la pista.
     */
    private String artista;
    /**
     * Género al que pertenece la canción de la pista.
     */
    private int idGenero;
    /**
     * Fecha de modificación del nombre de la canción de la pista.
     */
    private Timestamp fechaModificacionNombreCancion;
    /**
     * Fecha de modificación del artista de la pista.
     */
    private Timestamp fechaModificacionArtista;
    /**
     * Fecha de modificación del género de la pista.
     */
    private Timestamp fechaModificacionGenero;
    /**
     * Relaciona servidores con índices de descargas (para la replicación).
     */
    private HashMap estadoRelacionServidor;
    /**
     * Hash del fichero de la pista.
     */
    private String hash;
    /**
     * URL de la zona donde se encuentra la pista.
     */
    private String url;

    /**
     * No se permite la construcción de una Pista vacía.
     */
    private Pista() {
    }

    /**
     * Constructor básico con todas las características de una Pista.
     *
     * @param id Identificador del fichero de la pista.
     * @param nombreCancion Nombre de la canción de la pista.
     * @param artista Nombre del artista de la canción de la pista.
     * @param idGenero Género al que pertenece la canción de la pista.
     * @param fechaModificacionNombreCancion Fecha de modificación del fichero
     * de la pista.
     * @param fechaModificacionArtista Fecha de modificación del artista de la
     * pista.
     * @param fechaModificacionGenero Fecha de modificación del género de la
     * pista.
     * @param estadoRelacionServidor Relaciona servidores con índices de
     * descargas (para la replicación) [puede ser nulo].
     * @param hash Hash del fichero de la pista [puede ser nulo].
     * @param url URL del fichero de la pista [puede ser nulo].
     */
    public Pista(final int id, final String nombreCancion, final String artista, final int idGenero,
            final Timestamp fechaModificacionNombreCancion,
            final Timestamp fechaModificacionArtista, final Timestamp fechaModificacionGenero,
            final HashMap estadoRelacionServidor, final String hash, final String url) {
        // TODO: programación defensiva menos estadoRelacionServidor, hash y url
        this.id = id;
        this.nombreCancion = nombreCancion;
        this.artista = artista;
        this.idGenero = idGenero;
        this.fechaModificacionNombreCancion = fechaModificacionNombreCancion;
        this.fechaModificacionArtista = fechaModificacionArtista;
        this.fechaModificacionGenero = fechaModificacionGenero;
        // Puede ser nulo
        this.estadoRelacionServidor = estadoRelacionServidor;
        // Puede ser nulo
        this.hash = hash;
        // Puede ser nulo
        this.url = url;
    }

    /**
     * Obtiene el id de la pista.
     *
     * @return Id de la pila pista.
     */
    public int getId() {
        return this.id;
    }

    /**
     * Establece un nuevo valor del id de la pista.
     *
     * @param id Nuevo valor del id de la pista.
     */
    public void setId(final int id) {
        this.id = id;
    }

    /**
     * Obtiene el nombre de la canción de la pista.
     *
     * @return Nombre de la canción de la pista.
     */
    public String getNombreCancion() {
        return this.nombreCancion;
    }

    /**
     * Establece un nuevo nombre de la canción de la pista.
     *
     * @param nombreCancion Nuevo nombre a establecer.
     */
    public void setNombreCancion(final String nombreCancion) {
        this.nombreCancion = nombreCancion;
    }

    /**
     * Obtiene el mombre del artista de la canción de la pista.
     *
     * @return Nombre del artista de la canción de la pista.
     */
    public String getArtista() {
        return this.artista;
    }

    /**
     * Establece un nuevo mombre del artista de la canción de la pista.
     *
     * @param artista Nuevo mombre del artista de la canción de la pista.
     */
    public void setArtista(final String artista) {
        this.artista = artista;
    }

    /**
     * Obtiene el género al que pertenece la canción de la pista.
     *
     * @return Género al que pertenece la canción de la pista.
     */
    public int getIdGenero() {
        return this.idGenero;
    }

    /**
     * Establece un nuevo género al que pertenece la canción de la pista.
     *
     * @param idGenero Nuevo género a establecer.
     */
    public void setIdGenero(final int idGenero) {
        this.idGenero = idGenero;
    }

    /**
     * Obtiene la fecha de modificación del nombre de canción de la pista.
     *
     * @return Fecha de modificación del fichero de la pista.
     */
    public Timestamp getFechaModificacionNombreCancion() {
        return this.fechaModificacionNombreCancion;
    }

    /**
     * Establece una nueva fecha de modificación del nombre de canción de la
     * pista.
     *
     * @param fechaModificacionNombreCancion Nueva fecha de modificación a
     * establecer del nombre de la canción
     */
    public void setFechaModificacionNombreCancion(final Timestamp fechaModificacionNombreCancion) {
        this.fechaModificacionNombreCancion = fechaModificacionNombreCancion;
    }

    /**
     * Obtiene la fecha de modificación del artista de la pista.
     *
     * @return Fecha de modificación del artista de la pista.
     */
    public Timestamp getFechaModificacionArtista() {
        return fechaModificacionArtista;
    }

    /**
     * Establece una nueva fecha de modificación del artista de la pista.
     *
     * @param fechaModificacionArtista Nueva fecha de modificación a establecer
     * del artista.
     */
    public void setFechaModificacionArtista(final Timestamp fechaModificacionArtista) {
        this.fechaModificacionArtista = fechaModificacionArtista;
    }

    /**
     * Obtiene la fecha de modificación del género de la pista.
     *
     * @return Fecha de modificación del género de la pista.
     */
    public Timestamp getFechaModificacionGenero() {
        return fechaModificacionGenero;
    }

    /**
     * Esrablece la fecha de modificación del género de la pista.
     *
     * @param fechaModificacionGenero Nueva fecha de modificación a establecer
     * del género de la canción
     */
    public void setFechaModificacionGenero(final Timestamp fechaModificacionGenero) {
        this.fechaModificacionGenero = fechaModificacionGenero;
    }

    /**
     * Obtiene la relación con los servidores con índices de descargas (para la
     * replicación).
     *
     * @return Relación con los servidores con índices de descargas (para la
     * replicación).
     */
    public HashMap getEstadoRelacionServidor() {
        return this.estadoRelacionServidor;
    }

    /**
     * Establece una nueva relación con los servidores con índices de descargas
     * (para la replicación).
     *
     * @param estadoRelacionServidor Nuevos estados con el servidor.
     */
    public void setEstadoRelacionServidor(final HashMap estadoRelacionServidor) {
        this.estadoRelacionServidor = estadoRelacionServidor;
    }

    /**
     * Obtiene el hash del fichero de la pista.
     *
     * @return El hash del fichero de la pista.
     */
    public String getHash() {
        return hash;
    }

    /**
     * Establece un hash para la pista.
     *
     * @param hash Hash a establecer para la pista
     */
    public void setHash(final String hash) {
        this.hash = hash;
    }

    /**
     * Obtiene la URL del fichero de la pista.
     *
     * @return La URL del fichero de la pista.
     */
    public String getUrl() {
        return url;
    }

    /**
     * Establece la URL para la pista.
     *
     * @param hash la URL a establecer para la pista
     */
    public void setUrl(final String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "Pista{" + "id=" + id + ", nombreCancion=" + nombreCancion + ", artista=" + artista + ", idGenero=" + idGenero + ", fechaModificacionNombreCancion=" + ConstantsJSON.fechasFormatter.format(fechaModificacionNombreCancion) + ", fechaModificacionArtista=" + ConstantsJSON.fechasFormatter.format(fechaModificacionArtista) + ", fechaModificacionGenero=" + ConstantsJSON.fechasFormatter.format(fechaModificacionGenero) + ", estadoRelacionServidor=" + estadoRelacionServidor + ", hash=" + hash + ", url=" + url+ '}';
    }
}