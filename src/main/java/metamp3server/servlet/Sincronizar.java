package metamp3server.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import metamp3server.controller.AbstractController;
import metamp3server.controller.ControllerException;
import metamp3server.controller.IController;
import metamp3server.controller.SincronizarController;
import org.codehaus.jettison.json.JSONArray;

/**
 * Sincronizar (editar/actualizar) metadatos de n ficheros: /Sincronizar
 *
 * Intenta modificar los metadatos de los ficheros indicados (se permite si las
 * nuevas fechas son posteriores).
 *
 * Parámetros:
 *
 * id: id de la pista a editar
 *
 * nombre: nombre de la pista editado encoded.
 *
 * artista: artista de la pista editado encoded.
 *
 * idGenero: id del género de la pista encoded.
 *
 * fechaModiNombreCancion: fecha de modificación de la pista (formato yyyy-MM-dd
 * HH:mm:ss).
 *
 * fechaModiArtista: fecha de modificación de la pista (formato yyyy-MM-dd
 * HH:mm:ss).
 *
 * fechaModiGenero: fecha de modificación del género (formato yyyy-MM-dd
 * HH:mm:ss).
 *
 *
 * GET (ejemplo)
 *
 * /Sincronizar?pistas=[{"id":1212,"nombreCancion":"nombreEncoded1","artista":"artistaEncoded1","idGenero":20,"fechaModiNombreCancion":"2015-01-21 14:55:00","fechaModiArtista":"2015-01-21 14:55:00","fechaModiGenero":"2015-01-21 14:55:00"},{"id":123123,"nombreCancion":"Amigo","artista":"Pájaro","idGenero":30,"fechaModiNombreCancion":"2015-01-19 14:55:02","fechaModiArtista":"2015-01-20 14:55:03","fechaModiGenero":"2015-01-21 14:55:04"},{"id":12122323,"nombreCancion":"Otra","artista":"aqe","idGenero":20,"fechaModiNombreCancion":"2015-01-15 14:55:01","fechaModiArtista":"2015-01-26 14:55:02","fechaModiGenero":"2015-01-17 14:55:03"}]
 *
 * Respuesta (ejemplo): Fallo de una y éxito de otra:
 *
 * [{"estado":"fallo","mensaje":"No existe ninguna pista con ese
 * id"},{"estado":"exito","id": "4112","nombreCancion":
 * "nombreEncoded2","artista": "artistaEncoded2","idGenero":
 * "idGenero2","fechaModiNombreCancion": "2014-12-04
 * 12:00:01","fechaModiArtista": "2014-12-04 12:00:01","fechaModiGenero":
 * "2014-12-04 12:00:01","hash":"as121e1adsd" }]
 *
 * @author MetaMP3ASAI1415
 * @since 0.1-ALPHA
 * @version 0.1-ALPHA
 */
public final class Sincronizar extends HttpServlet {

    protected void processRequest(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {

        try {
            request.setCharacterEncoding("UTF-8");
            final IController mController = new SincronizarController();
            mController.init(request);
            mController.execute();

            response.setContentType("application/json;charset=UTF-8");
            final PrintWriter out = response.getWriter();
            final JSONArray objetoJSON = (JSONArray) request.getAttribute(AbstractController.OBJETO_JSON);
            out.print(objetoJSON != null ? objetoJSON : "");
            out.flush();
        } catch (final ControllerException ce) {
            System.out.println("Error en Sincronizar\n" + ce.getMessage());
        }
    }

    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}