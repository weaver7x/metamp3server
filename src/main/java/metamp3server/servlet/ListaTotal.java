package metamp3server.servlet;


import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import metamp3server.controller.AbstractController;
import metamp3server.controller.ControllerException;
import metamp3server.controller.IController;
import metamp3server.controller.ListaTotalController;
import org.codehaus.jettison.json.JSONArray;

/**
 * Listar todos los ficheros en el servidor: /ListaTotal
 *
 * Parámetros:Ninguno
 *
 * GET (ejemplo)
 *
 * /ListaTotal
 *
 * Respuesta (ejemplo): Array JSON encoded de pistas.
 *
 * @author MetaMP3ASAI1415
 * @since 0.1-ALPHA
 * @version 0.1-ALPHA
 */
public final class ListaTotal extends HttpServlet {

    protected void processRequest(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {

        try {
            request.setCharacterEncoding("UTF-8");
            final IController mController = new ListaTotalController();
            mController.init(request);
            mController.execute();

            response.setContentType("application/json;charset=UTF-8");
            final PrintWriter out = response.getWriter();
            final JSONArray objetoJSON = (JSONArray) request.getAttribute(AbstractController.OBJETO_JSON);
            out.print(objetoJSON != null ? objetoJSON : "");
            out.flush();
        } catch (final ControllerException ce) {
            System.out.println("Error en ListaTotal\n" + ce.getMessage());
        }
    }

    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}