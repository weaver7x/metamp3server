package metamp3server.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import metamp3server.controller.ConstantsJSON;
import metamp3server.entity.Pista;

/**
 * Operaciones en la base de datos con {@link Pista}. Patrón DAO.
 *
 * @author MetaMP3ASAI1415
 * @since 0.1-ALPHA
 * @version 0.1-ALPHA
 */
final class PistaDAO {

    /**
     * Obtiene todos los datos de las pistas especificados por su ID, excepto la
     * relación en los servidores y la url de descarga. Esto se hace así por
     * eficiencia en la búsqueda,cuando no es necesario saber en qué servidores
     * se encuentra la pista ni su url de descarga.
     *
     * @param ids ID's de las pistas a buscar.
     * @return Lista de pistas con sus datos excepto la relación en los
     * servidores.
     * @throws IllegalArgumentException Si la lista de ID's de nula.
     */
    static List<Pista> getPistas(final List<Integer> ids) throws IllegalArgumentException {
        if (ids == null) {
            throw new IllegalArgumentException("ids no puede ser nulo");
        }
        final ConnectionPool pool = ConnectionPool.getInstance();
        final Connection con = pool.getConnection();
        PreparedStatement ps;
        final ArrayList<Pista> pistas = new ArrayList<Pista>();
        try {
            // Pasar a modo MySQL: (num1,num2,...,numSize_1)
            final StringBuilder idsString = new StringBuilder();
            final int idsSize_1 = ids.size() - 1;
            for (int i = 0; i < idsSize_1; i++) {
                idsString.append(ids.get(i)).append(',');
            }
            if (idsSize_1 >= 0) {
                idsString.append(ids.get(idsSize_1));
            }


            ps = con.prepareStatement(new StringBuilder("SELECT * FROM ").append(Constants.VISTA_PISTAS).
                    append(" WHERE ").append(Constants.PISTA_ID_PISTA).append(" IN (").append(idsString).append(")").toString());
            final ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                pistas.add(new Pista(rs.getInt(Constants.PISTA_ID_PISTA), rs.getString(Constants.PISTA_NOMBRE_CANCION),
                        rs.getString(Constants.PISTA_ARTISTA), rs.getInt(Constants.PISTA_GENERO_FK),
                        rs.getTimestamp(Constants.PISTA_FECHA_MOD_NOMBRE_CANCION), rs.getTimestamp(Constants.PISTA_FECHA_MOD_ARTISTA),
                        rs.getTimestamp(Constants.PISTA_FECHA_MOD_GENERO), null, rs.getString(Constants.PISTA_HASH), null));
            }
        } catch (final Exception e) {
            //e.printStackTrace();
        } finally {
            if (con != null) {
                pool.freeConnection(con);
            }
        }
        return pistas;
    }

    /**
     * Obtiene todos los datos de las pistas especificados por su ID, incluyendo
     * la relación en los servidores y la url de descarga.
     *
     * @param ids ID's de las pistas a buscar.
     * @return Lista de pistas con sus datos junto a la relación en los
     * servidores.
     * @throws IllegalArgumentException Si la lista de ID's de nula.
     */
    static List<Pista> getPistasRelacion(final List<Integer> ids, final int idZonaCliente) throws IllegalArgumentException {
        if (ids == null) {
            throw new IllegalArgumentException("ids no puede ser nulo");
        }
        final ConnectionPool pool = ConnectionPool.getInstance();
        final Connection con = pool.getConnection();
        PreparedStatement ps;
        final ArrayList<Pista> pistas = new ArrayList<Pista>();
        try {
            // Pasar a modo MySQL: (num1,num2,...,numSize_1)
            final StringBuilder idsString = new StringBuilder();
            final int idsSize_1 = ids.size() - 1;
            for (int i = 0; i < idsSize_1; i++) {
                idsString.append(ids.get(i)).append(',');
            }
            if (idsSize_1 >= 0) {
                idsString.append(ids.get(idsSize_1));
            }

            ps = con.prepareStatement(new StringBuilder("SELECT * FROM ").append(Constants.VISTA_PISTAS_SERVIDORES).
                    append(" WHERE ").append(Constants.PISTA_ID_PISTA).append(" IN (").append(idsString).append(")").toString());
            final ResultSet rs = ps.executeQuery();
            final HashSet<Integer> idsPistas = new HashSet();
            Integer idActual;
            HashMap relacionesAux;
            Pista pistaAux;
            while (rs.next()) {
                idActual = rs.getInt(Constants.PISTA_ID_PISTA);
                if (idsPistas.add(idActual)) {
                    pistas.add(new Pista(idActual, rs.getString(Constants.PISTA_NOMBRE_CANCION),
                            rs.getString(Constants.PISTA_ARTISTA), rs.getInt(Constants.PISTA_GENERO_FK),
                            rs.getTimestamp(Constants.PISTA_FECHA_MOD_NOMBRE_CANCION), rs.getTimestamp(Constants.PISTA_FECHA_MOD_ARTISTA),
                            rs.getTimestamp(Constants.PISTA_FECHA_MOD_GENERO), new HashMap(), rs.getString(Constants.PISTA_HASH), null));
                }
                relacionesAux = buscaPista(pistas, idActual).getEstadoRelacionServidor();
                relacionesAux.put(rs.getInt(Constants.PISTA_SERVIDOR_DATOS_SERVIDOR_DATOS_FK), rs.getInt(Constants.PISTA_SERVIDOR_DATOS_SERVIDOR_ESTADO_RELACION));
                // Añadir la url si la zona del fichero coincide con la zona del cliente o si no había URL
                pistaAux = buscaPista(pistas, idActual);
                if ((idZonaCliente == rs.getInt(Constants.PISTA_SERVIDOR_DATOS_SERVIDOR_DATOS_FK) || pistaAux.getUrl() == null) && rs.getInt(Constants.PISTA_SERVIDOR_DATOS_SERVIDOR_ESTADO_RELACION) == ConstantsJSON.FICHERO_EN_SERVIDOR) {
                    pistaAux.setUrl(rs.getString(Constants.PISTA_SERVIDOR_DATOS_SERVIDOR_URI) + idActual + ".mp3");
                }
            }
        } catch (final Exception e) {
            //e.printStackTrace();
        } finally {
            if (con != null) {
                pool.freeConnection(con);
            }
        }
        return pistas;
    }

    private static Pista buscaPista(final List<Pista> pistas, int idPista) {
        boolean encontrada = false;
        int index = pistas.size() - 1;
        Pista pistaEncontrada = null;
        while (index >= 0 && !encontrada) {
            pistaEncontrada = pistas.get(index);
            if (pistaEncontrada.getId() == idPista) {
                encontrada = true;
            }
            index--;
        }
        return encontrada ? pistaEncontrada : null;
    }

    /**
     * Obtiene la lista de todas las pistas de la aplicación, sin las relaciones
     * del servidor ni la url de descarga.
     *
     * @return Lista de todas las pistas de la aplicación, sin las relaciones
     * del servidor ni la url de descarga.
     * @throws IllegalArgumentException
     */
    static ArrayList<Pista> getTodasPistas() {
        final ConnectionPool pool = ConnectionPool.getInstance();
        final Connection con = pool.getConnection();
        PreparedStatement ps;
        final ArrayList<Pista> pistas = new ArrayList<Pista>();
        try {
            ps = con.prepareStatement(new StringBuilder("SELECT * FROM ").append(Constants.VISTA_PISTAS).toString());
            final ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                pistas.add(new Pista(rs.getInt(Constants.PISTA_ID_PISTA), rs.getString(Constants.PISTA_NOMBRE_CANCION),
                        rs.getString(Constants.PISTA_ARTISTA), rs.getInt(Constants.PISTA_GENERO_FK),
                        rs.getTimestamp(Constants.PISTA_FECHA_MOD_NOMBRE_CANCION), rs.getTimestamp(Constants.PISTA_FECHA_MOD_ARTISTA),
                        rs.getTimestamp(Constants.PISTA_FECHA_MOD_GENERO), null, rs.getString(Constants.PISTA_HASH), null));
            }
        } catch (final Exception e) {
            //e.printStackTrace();
        } finally {
            if (con != null) {
                pool.freeConnection(con);
            }
        }
        return pistas;
    }

    /**
     *
     * @param idPista Id de la pista a cambiar sus relaicones.
     * @param idServidorDatos Id del servidor a cambiar las relaciones con la
     * pista.
     * @param nuevaRelacion Valor numérica de la relación entre la pista y el
     * servidor.
     * @return {@link Constants.SQL_OK} si todo fue bien o
     * {@link Constants.SQL_ERROR} si hubo error.
     */
    static byte updateRelaciones(final int idPista, final int idServidorDatos, final int nuevaRelacion) {
        final ConnectionPool pool = ConnectionPool.getInstance();
        final Connection con = pool.getConnection();
        try {
            final PreparedStatement ps = con.prepareStatement(new StringBuilder("UPDATE ")
                    .append(Constants.TABLA_PISTA_SERVIDOR_DATOS)
                    .append(" SET ")
                    .append(Constants.PISTA_SERVIDOR_DATOS_SERVIDOR_ESTADO_RELACION).append("=? WHERE ")
                    .append(Constants.PISTA_SERVIDOR_DATOS_SERVIDOR_DATOS_FK).append("= ? AND ")
                    .append(Constants.PISTA_SERVIDOR_DATOS_SERVIDOR_PISTA_FK).append(" =?")
                    .toString());
            ps.setInt(1, nuevaRelacion);
            ps.setInt(2, idServidorDatos);
            ps.setInt(3, idPista);
            ps.executeUpdate();
        } catch (final Exception e) {
            //e.printStackTrace();
            return Constants.SQL_ERROR;
        } finally {
            if (con != null) {
                pool.freeConnection(con);
            }
            return Constants.SQL_OK;
        }
    }

    /**
     * Actualiza los metadatos y sus fechas de modificaicón de una pista en la
     * base de datos.
     *
     * @param pista Pista con idPista, nombreCancion, artista, genero y sus tres
     * fechas de modificaicon.
     * @return La pista con todos sus metadatos actualizados y las relaciones
     * con los servidores de datos (sin URL de descarga) o "null" si hubo algún
     * error.
     * @throws IllegalArgumentException Si la lista de ID's de nula.
     */
    static Pista updatePistaMetadatos(final Pista pista) throws IllegalArgumentException {
        if (pista == null) {
            throw new IllegalArgumentException("La pista no peude ser nula.");
        }
        final ConnectionPool pool = ConnectionPool.getInstance();

        final Connection con = pool.getConnection();
        try {
            PreparedStatement ps = con.prepareStatement(new StringBuilder("UPDATE ")
                    .append(Constants.TABLA_PISTA)
                    .append(" SET ")
                    .append(Constants.PISTA_NOMBRE_CANCION).append("=? ,")
                    .append(Constants.PISTA_ARTISTA).append("=? ,")
                    .append(Constants.PISTA_GENERO_FK).append("=? ,")
                    .append(Constants.PISTA_FECHA_MOD_NOMBRE_CANCION).append("=? ,")
                    .append(Constants.PISTA_FECHA_MOD_ARTISTA).append("=? ,")
                    .append(Constants.PISTA_FECHA_MOD_GENERO)
                    .append("=? WHERE ")
                    .append(Constants.PISTA_ID_PISTA).append("=?")
                    .toString());
            ps.setString(1, pista.getNombreCancion());
            ps.setString(2, pista.getArtista());
            ps.setInt(3, pista.getIdGenero());
            ps.setTimestamp(4, pista.getFechaModificacionNombreCancion());
            ps.setTimestamp(5, pista.getFechaModificacionArtista());
            ps.setTimestamp(6, pista.getFechaModificacionGenero());
            final int pistaId = pista.getId();
            ps.setInt(7, pistaId);

            ps.executeUpdate();

            // 2. Obtiene los datos de servidores donde se encuentra
            ps = con.prepareStatement(new StringBuilder("SELECT * FROM ").append(Constants.VISTA_PISTAS_SERVIDORES).
                    append(" WHERE ").append(Constants.PISTA_ID_PISTA).append(" IN (").append(pistaId).append(")").toString());
            final ResultSet rs = ps.executeQuery();
            final HashMap relacionesAux = new HashMap();
            while (rs.next()) {
                relacionesAux.put(rs.getInt(Constants.PISTA_SERVIDOR_DATOS_SERVIDOR_DATOS_FK), rs.getInt(Constants.PISTA_SERVIDOR_DATOS_SERVIDOR_ESTADO_RELACION));
            }
            pista.setEstadoRelacionServidor(relacionesAux);

        } catch (final Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (con != null) {
                pool.freeConnection(con);
            }
            return pista;
        }
    }
}