package metamp3server.dao;

import java.util.List;
import metamp3server.entity.Pista;

/**
 * Interfaz fachada a seguir por los clientes que deseen comunicarse con la base
 * de datos. Patrón Façade.
 *
 * @author MetaMP3ASAI1415
 * @since 0.1-ALPHA
 * @version 0.1-ALPHA
 */
public interface IPersistencia {

    /**
     * Obtiene todas las pistas de la base de datos.
     *
     * @return Lista de todas las pistas de la base de datos.
     */
    public List<Pista> getTodasPistas();

    /**
     * Obtiene todas las pistas de la base de datos que coincidan con los ids
     * proporcionados sin las relaciones con los servidores.
     *
     * @return Lista de todas las pistas de la base de datos que coincidan con
     * los ids proporcionados.
     */
    public List<Pista> getPistas(final List<Integer> idsPistas);

    /**
     * Obtiene todas las pistas de la base de datos que coincidan con los ids
     * proporcionados con las relaciones con los servidores.
     *
     * @return Lista de todas las pistas de la base de datos que coincidan con
     * los ids proporcionados.
     */
    public List<Pista> getPistasRelacion(final List<Integer> idsPistas, final int idZonaCliente);

    /**
     *
     * @param idPista Id de la pista a cambiar sus relaicones.
     * @param idServidorDatos Id del servidor a cambiar las relaciones con la
     * pista.
     * @param nuevaRelacion Valor numérica de la relación entre la pista y el
     * servidor.
     * @return {@link Constants.SQL_OK} si todo fue bien o
     * {@link Constants.SQL_ERROR} si hubo error.
     */
    public byte updateRelaciones(final int idPista, final int idServidorDatos, final int nuevaRelacion);

    /**
     * Actualiza los metadatos y sus fechas de modificaicón de una pista en la
     * base de datos.
     *
     * @param pista Pista con idPista, nombreCancion, artista, genero y sus tres
     * fechas de modificaicon.
     * @return La pista con todos sus metadatos actualizados y los demás datos o
     * "null" si hubo algún error.
     */
    public Pista updatePistaMetadatos(final Pista pista);
}