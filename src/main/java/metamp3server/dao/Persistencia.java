package metamp3server.dao;

import java.util.List;
import metamp3server.entity.Pista;

/**
 * Implementación de los métodos para acceder a la base de datos.
 *
 * @author MetaMP3ASAI1415
 * @since 0.1-ALPHA
 * @version 0.1-ALPHA
 */
public class Persistencia implements IPersistencia {

    @Override
    public List<Pista> getTodasPistas() {
        System.out.println("\n/ListaTotal muestro todas");
        return PistaDAO.getTodasPistas();
    }

    @Override
    public List<Pista> getPistas(final List<Integer> idsPistas) {
        return PistaDAO.getPistas(idsPistas);
    }

    @Override
    public List<Pista> getPistasRelacion(final List<Integer> idsPistas, final int idZonaCliente) {
        return PistaDAO.getPistasRelacion(idsPistas, idZonaCliente);
    }

    @Override
    public byte updateRelaciones(final int idPista, final int idServidorDatos, final int nuevaRelacion) {
        return PistaDAO.updateRelaciones(idPista, idServidorDatos, nuevaRelacion);
    }

    @Override
    public Pista updatePistaMetadatos(final Pista pista) {
        return PistaDAO.updatePistaMetadatos(pista);
    }
}