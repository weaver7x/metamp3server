/**
 * <p>
 * Clases que acceden y manipulan objetos relacionados con la base de datos.
 * </p>
 * Ver Java DAO pattern para más detalles.
 *
 * @author MetaMP3ASAI1415
 * @since 0.1-ALPHA
 * @version 0.1-ALPHA
 */
package metamp3server.dao;