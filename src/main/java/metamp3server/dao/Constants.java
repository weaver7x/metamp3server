/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package metamp3server.dao;

/**
 *
 * @author MetaMP3ASAI1415
 * @since 0.1-ALPHA
 * @version 0.1-ALPHA
 */
public class Constants {

    private Constants() {
    }
    final static String VISTA_PISTAS = "Pistas";
    final static String PISTA_ID_PISTA = "idPista";
    final static String PISTA_NOMBRE_CANCION = "nombreCancion";
    final static String PISTA_HASH = "hash";
    final static String PISTA_ARTISTA = "artista";
    final static String PISTA_GENERO_FK = "generoFK";
    final static String PISTA_FECHA_MOD_NOMBRE_CANCION = "fechaModNombreCancion";
    final static String PISTA_FECHA_MOD_ARTISTA = "fechaModArtista";
    final static String PISTA_FECHA_MOD_GENERO = "fechaModGenero";
    final static String VISTA_PISTAS_SERVIDORES = "PistasServidores";
    final static String PISTA_SERVIDOR_DATOS_SERVIDOR_DATOS_FK = "servidorDatosFK";
    final static String PISTA_SERVIDOR_DATOS_SERVIDOR_ESTADO_RELACION = "estadoRelacion";
    final static String PISTA_SERVIDOR_DATOS_SERVIDOR_PISTA_FK = "pistaFK";
    final static String PISTA_SERVIDOR_DATOS_SERVIDOR_URI = "uri";
    final static String TABLA_PISTA_SERVIDOR_DATOS = "PistaServidorDatos";
    final static String TABLA_PISTA = "Pista";
    /**
     * Indica que hubo un error al aplicar la sentencia SQL.
     */
    public final static byte SQL_ERROR = -1;
    /**
     * Indica que se ejecutó correctamente la sentencia SQL.
     */
    public final static byte SQL_OK = 0;
}