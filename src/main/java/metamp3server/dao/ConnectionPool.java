package metamp3server.dao;

import java.sql.Connection;
import java.sql.SQLException;
import javax.sql.DataSource;
import org.apache.commons.dbcp.BasicDataSource;

/**
 * Pool de conexiones a la base de datos. Crea un único acceso (pool) a la base
 * de datos de MetaMP3BD (eficiencia en vez abrir una comunicación en cada acceso).
 * Patrón Singleton.
 *
 * @author MetaMP3ASAI1415
 * @since 0.1-ALPHA
 * @version 0.1-ALPHA
 */
final class ConnectionPool {

    /**
     * Pool de conexiones.
     */
    private static ConnectionPool pool;
    /**
     * Datasource.
     */
    private static DataSource dataSource;

    /**
     * Constructor privado.
     */
    private ConnectionPool() {
        try {
            dataSource = MetaMP3DataSource.getDataBase();
        } catch (final Exception e) {
        }
    }

    /**
     * Inicializa un pool de conexiones a MetaMP3BD.
     *
     * @return Pool de conexiones a MetaMP3BD.
     */
    public static ConnectionPool getInstance() {
        if (pool == null) {
            pool = new ConnectionPool();
        }
        return pool;
    }

    /**
     * Obtiene una conexión a MetaMP3BD.
     * @return Conexión a MetaMP3BD o NULL si no se pudo realizar.
     */
    public Connection getConnection() {
        try {
            return dataSource.getConnection();
        } catch (final SQLException sqle) {
            //sqle.printStackTrace();
            return null;
        }
    }

    /**
     * Cierra el pool de conexiones a MetaMP3BD.
     *
     * @param c Conexión a cerrar.
     */
    public final void freeConnection(final Connection c) {
        try {
            c.close();
        } catch (final SQLException sqle) {
            // Se considera cerrada si hubo algún problema
        }
    }

    /**
     * Configura MetaMP3BD. Sólo se utilizará esta base de datos.
     */
    private static final class MetaMP3DataSource {

        /**
         * Obtiene la configuración de la base de datos MetaMP3BD.
         *
         * @return Configuración de la base de datos MetaMP3BD.
         */
        static BasicDataSource getDataBase() throws SQLException {
            final BasicDataSource basicDataSource = new BasicDataSource();
            basicDataSource.setUrl("jdbc:mysql://pasarela.lab.inf.uva.es:30024/MetaMP3BD?reconnect=true");
            basicDataSource.setUsername("usersql");
            basicDataSource.setPassword("usersql");
            basicDataSource.setValidationQuery("SELECT 1");
            // Máximas conexiones a la vez, activar si fuera necesario
            // basicDataSource.setMaxActive(10);
            return basicDataSource;
        }
    }
}