package metamp3server.controller;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import metamp3server.dao.Persistencia;
import metamp3server.entity.Pista;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 * Encargado de la replicación correcta de pistas.
 *
 * @author MetaMP3ASAI1415
 * @since 0.1-ALPHA
 * @version 1.0-FINAL
 */
final class GestorReplicacion {

    /**
     * Decide si replicar o no un conjunto de pistas dado un cliente que ha
     * pedido sus descargas.
     *
     * @param pistas Pistas a replicar.
     * @param zonaCliente Zona del cliente que pide las pistas.
     * @throws JSONException Si hubo algún error de comunicación en la
     * replicación.
     */
    public void replica(final List<Pista> pistas, final int zonaCliente) throws JSONException {
        HashMap map;
        boolean encontrado;
        Map.Entry<Integer, Integer> entrada;
        Iterator<Map.Entry<Integer, Integer>> it;
        final byte NO_SERVER = -1;
        int idPista, relacionServidor;
        JSONObject respuestaIdentificacion;
        String webTomcat, auxWeb;

        for (final Pista pista : pistas) {
            idPista = pista.getId();
            System.out.println("\n " + idPista + ' ' + pista.getNombreCancion());
            map = pista.getEstadoRelacionServidor();


            // Se busca un servidor que tenga el fichero, para posibles replicaciones
            int idServerFichero = NO_SERVER;
            encontrado = false;
            it = map.entrySet().iterator();
            while (it.hasNext() && !encontrado) {
                entrada = it.next();
                if (entrada.getValue() == ConstantsJSON.FICHERO_EN_SERVIDOR) {
                    idServerFichero = entrada.getKey();
                    encontrado = true;
                }
            }


            System.out.println("\n  --> idServerFichero: " + idServerFichero + " zonaCliente: " + zonaCliente);
            if (idServerFichero != NO_SERVER && idServerFichero != zonaCliente) {
                relacionServidor = (Integer) map.get(zonaCliente);

                //for (final Map.Entry<Integer, Integer> entry : hSet) {
                System.out.println("\n  --> Servidor: " + zonaCliente + " Valor: " + relacionServidor);

                if (relacionServidor == ConstantsJSON.MAXIMO_NUMERO_DESCARGAS_CAMBIO - 1) {

                    System.out.println("\t-- SÍ Replicación (tras descarga) a otro servidor de backup + relación a " + ConstantsJSON.FICHERO_EN_SERVIDOR);
                    System.out.println("\t\t-- 1º Servidor0 apunta el inicio de operación de edición de relaciones del fichero en el Gestor de Recuperaciones.");
                    System.out.println("\t\t-- 2º Servidor0 llama a la base de datos para que cambie las relaciones a " + ConstantsJSON.FICHERO_EN_SERVIDOR);

                    // TODO: si hubo un error, no seguir: Gestor de recuperaciones
                    System.out.println("\t **Gestor de recuperación -> Actualizar relaciones BD: prepared.**");
                    new Persistencia().updateRelaciones(idPista, zonaCliente, ConstantsJSON.FICHERO_EN_SERVIDOR);


                    //System.out.println("\t\t-- 2ºbis Servidor0 no recibe nada --> Otra vez se reintentará en el futuro desde el 5º paso.");
                    System.out.println("\t\t-- 3º Respuesta OK.");
                    System.out.println("\t **Gestor de recuperación -> Actualizar relaciones BD: committed.**");


                    // TODO: si hubo un error, no seguir: Gestor de recuperaciones
                    System.out.println("\t **Gestor de recuperación -> Replicación: prepared.**");
                    System.out.println("\t\t-- 4º Servidor0 apunta el inicio de la operación de replicación en el Gestor de Recuperaciones.");
                    System.out.println("\t\t-- 5º Servidor0 le dice al servidor de backup " + idServerFichero + " que replique y espera respuesta inmediata.");
                    auxWeb = pista.getUrl(); // esto me da por ejemplo http://pasarela.lab.inf.uva.es:30032/webdav/123123.mp3

                    System.out.println("\t\t\t.. Me identifico por seguridad.");
                    webTomcat = auxWeb.substring(0, auxWeb.indexOf(ConstantsJSON.WEB_DAV) - 2) + ConstantsJSON.WEB_DAV_PUERTO;
                    respuestaIdentificacion = new JSONObject(HttpConnection.conexionHttp(webTomcat + "/Identificacion?" + ConstantsJSON.BACKUP_API_KEY_ID + "=1&" + ConstantsJSON.BACKUP_API_KEY_PASS + "=servidor0", "POST").toString());
                    if (respuestaIdentificacion.optString(ConstantsJSON.BACKUP_API_KEY_ESTADO).equals(ConstantsJSON.BACKUP_API_KEY_ESTADO_EXITO)) {
                        System.out.println("\t\t\t.. Identificado correctamente");
                        System.out.println("\t\t\t.. Replicando con: " + webTomcat + "/Replicacion?" + ConstantsJSON.BACKUP_API_KEY_TOKEN + '=' + respuestaIdentificacion.optString(ConstantsJSON.BACKUP_API_KEY_TOKEN) + '&' + ConstantsJSON.PISTA_API_KEY_ID + '=' + idPista + '&' + ConstantsJSON.PISTA_API_KEY_ZONA + '=' + zonaCliente); // 3 es el puerto 8080 http para las conexiones http
                        HttpConnection.conexionHttp( //el 3 es el puerto de Tomcat en las máquinas virtuales (8080)
                                webTomcat + "/Replicacion?" + ConstantsJSON.BACKUP_API_KEY_TOKEN + '=' + respuestaIdentificacion.optString(ConstantsJSON.BACKUP_API_KEY_TOKEN) + '&' + ConstantsJSON.PISTA_API_KEY_ID + '=' + idPista + '&' + ConstantsJSON.PISTA_API_KEY_ZONA + '=' + zonaCliente, "POST");


                        System.out.println("\t\t-- 6º Respuesta OK.");
                        //System.out.println("\t\t-- 6ºbis Servidor0 no recibe nada --> Otra vez se reintentará en el futuro desde el 6º paso.");
                        System.out.println("\t\t-- 7º Servidor0 borra la entrada del Gestor de Recuperaciones => Transacción completada con éxito.");
                        System.out.println("\t **Gestor de recuperación -> Replicación: committed.**");
                    }
                } else {
                    // Si es 0, 1...
                    // ConstantsJSON.MAXIMO_NUMERO_DESCARGAS_CAMBIO-2 se
                    // cambian sus valores en la base de datos.
                    if (relacionServidor > ConstantsJSON.FICHERO_EN_SERVIDOR) {
                        relacionServidor += 1;
                        System.out.println("\t-- NO replicación establezco relación del server " + zonaCliente + " en: " + relacionServidor);
                        System.out.println("\t-- Método de cambio de relaciones (tras descarga)");

                        System.out.println("\t\t-- 1º Servidor0 apunta el inicio de la operación en el Gestor de Recuperaciones.");
                        System.out.println("\t\t-- 2º Servidor0 llama a la base de datos para que cambie las relaciones a " + relacionServidor);

                        // TODO: si hubo un error, no seguir: Gestor de recuperaciones
                        System.out.println("\t **Gestor de recuperación -> Actualizar relaciones BD: prepared.**");
                        new Persistencia().updateRelaciones(idPista, zonaCliente, relacionServidor);
                        //System.out.println("\t\t-- 2ºbis Servidor0 no recibe nada --> Otra vez se reintentará en el futuro desde el 2º paso.");
                        System.out.println("\t\t-- 3º Respuesta OK.");
                        System.out.println("\t\t-- 4º Servidor0 borra la entrada del Gestor de Recuperaciones => Transacción completada con éxito.");
                        System.out.println("\t **Gestor de recuperación -> Actualizar relaciones BD: committed.**");
                    }
                    // }else{valorAux=ConstantsJSON.FICHERO_EN_SERVIDOR: no hace falta aumentar su contador}
                }
            }

        }
    }
}