package metamp3server.controller;

import metamp3server.dao.Persistencia;

/**
 *
 * Controlador del metodo de la API del servidor /ListaTotal
 *
 * @author MetaMP3ASAI1415
 * @since 0.1-ALPHA
 * @version 0.1-ALPHA
 */
public final class ListaTotalController extends AbstractController {

    @Override
    public void execute() throws ControllerException {
        this.request.setAttribute(OBJETO_JSON, UtilsJSON.pistasToList(new Persistencia().getTodasPistas(),false));
    }
}