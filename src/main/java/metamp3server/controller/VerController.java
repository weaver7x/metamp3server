package metamp3server.controller;

import java.util.ArrayList;
import java.util.List;
import metamp3server.dao.Persistencia;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 * Controlador del metodo de la API del servidor /Ver
 *
 * @author MetaMP3ASAI1415
 * @since 0.1-ALPHA
 * @version 0.1-ALPHA
 */
public final class VerController extends AbstractController {

    @Override
    public void execute() throws ControllerException {
        final String idsJSON = this.request.getParameter(ConstantsJSON.PISTA_API_KEY_ID);
        try {
            if (idsJSON != null) {
                final JSONArray idsPistasJSON = new JSONArray(idsJSON);
                final int numeroPistas = idsPistasJSON.length();
                System.out.println("\n/Ver " + numeroPistas + " pistas");

                final List<Integer> idsPistas = new ArrayList<Integer>();
                for (int i = 0; i < numeroPistas; i++) {
                    idsPistas.add(idsPistasJSON.getInt(i));
                }
                this.request.setAttribute(OBJETO_JSON, UtilsJSON.pistasToList(new Persistencia().getPistas(idsPistas), false));

            } else {
                this.request.setAttribute(OBJETO_JSON, new JSONArray().put(new JSONObject().put(ConstantsJSON.PISTA_KEY_ESTADO, ConstantsJSON.PISTA_KEY_ESTADO_FALLO).put(ConstantsJSON.PISTA_KEY_ESTADO_MENSAJE, "Se deben especificar los ids de las pistas a visualizar.")));
            }
        } catch (final JSONException ex) {
            ex.printStackTrace();
        }
    }
}