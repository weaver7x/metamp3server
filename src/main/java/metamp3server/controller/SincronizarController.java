package metamp3server.controller;

import java.net.URLEncoder;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import metamp3server.dao.Persistencia;
import metamp3server.entity.Pista;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 * Controlador del metodo de la API del servidor /Sincronizar
 *
 * @author MetaMP3ASAI1415
 * @since 0.1-ALPHA
 * @version 1.0-FINAL
 */
public final class SincronizarController extends AbstractController {

    @Override
    public void execute() throws ControllerException {
        final String pistasJSON = this.request.getParameter(ConstantsJSON.PISTA_KEY_SINCRONIZAR_PISTAS);
        boolean hayError = false;
        try {
            if (pistasJSON != null) {

                // 1. Obtener los datos de las pistas en objetos "entity"
                // Nota: no se hacen los cambios en la base de datos a la vez por seguridad
                // Primero se comprueba que los datos pasado en el JSON con correctos, y después se actualiza la base de datos.
                final JSONArray pistasModificadasJSON = new JSONArray(pistasJSON);
                final int numeroPistas = pistasModificadasJSON.length();
                System.out.println("\n/Sincronizar " + numeroPistas + " pistas");

                final List<Pista> pistasModificadas = new ArrayList<Pista>();
                JSONObject pistaActual;
                final List<Integer> idsPistas = new ArrayList<Integer>();
                int idPista;

                for (int i = 0; i < numeroPistas; i++) {
                    pistaActual = pistasModificadasJSON.getJSONObject(i);
                    idPista = pistaActual.getInt(ConstantsJSON.PISTA_API_KEY_ID);
                    idsPistas.add(idPista);
                    pistasModificadas.add(new Pista(idPista,
                            pistaActual.getString(ConstantsJSON.PISTA_KEY_NOMBRE_CANCION),
                            pistaActual.getString(ConstantsJSON.PISTA_KEY_ARTISTA),
                            pistaActual.getInt(ConstantsJSON.PISTA_KEY_ID_GENERO),
                            new Timestamp(ConstantsJSON.fechasFormatter.parse(pistaActual.getString(ConstantsJSON.PISTA_KEY_FECHA_MOD_NOMBRE_CANCION)).getTime()),
                            new Timestamp(ConstantsJSON.fechasFormatter.parse(pistaActual.getString(ConstantsJSON.PISTA_KEY_FECHA_MOD_ARTISTA)).getTime()),
                            new Timestamp(ConstantsJSON.fechasFormatter.parse(pistaActual.getString(ConstantsJSON.PISTA_KEY_FECHA_MOD_GENERO)).getTime()),
                            null, null, null));
                }

                // 2. Obtener los datos de las pistas pedidas por el cliente en la base de datos
                final List<Pista> pistasBBDD = new Persistencia().getPistas(idsPistas);
                final int pistasBBDDSize = pistasBBDD.size();

                // 3. Para cada pista a intentar modificar
                // 3.1 Buscarla en las de la nube nube para ver si existe
                // 3.2 Comparar las fechas de edición de metadatos
                boolean encontrada;
                int cont, idPistaModificadaActual;
                Pista pistaActualBBDD;
                JSONArray respuesta = new JSONArray();
                ArrayList<Thread> mThreads = new ArrayList<Thread>();
                Thread mThread;
                final Date fechaActual = new Date();
                for (final Pista pistaModificada : pistasModificadas) {
                    encontrada = false;
                    cont = 0;
                    idPistaModificadaActual = pistaModificada.getId();
                    while (cont < pistasBBDDSize && !encontrada) {
                        pistaActualBBDD = pistasBBDD.get(cont);
                        if (idPistaModificadaActual == pistaActualBBDD.getId()) {
                            // Se encuentra en la base de datos
                            encontrada = true;
                            System.out.println("\tLa pista: " + idPistaModificadaActual + " se encuentra en la BBDD. Se comparan fechas:");
                            // Lanzo hilo
                            mThread = new ThreadComparador(pistaActualBBDD, pistaModificada, fechaActual, respuesta);
                            mThread.start();
                            mThreads.add(mThread);
                            // Esperar a que se terminen todos de ejecutar
                            for (final Thread current : mThreads) {
                                current.join();
                            }
                        }
                        cont++;
                    }
                    if (!encontrada) {
                        System.out.println("\tLa pista: " + idPistaModificadaActual + " no se encuentra en la BBDD.");
                        respuesta.put(new JSONObject().put(ConstantsJSON.PISTA_KEY_ID, idPistaModificadaActual).put(ConstantsJSON.PISTA_KEY_ESTADO, ConstantsJSON.PISTA_KEY_ESTADO_FALLO).put(ConstantsJSON.PISTA_KEY_ESTADO_MENSAJE, "La pista: " + idPistaModificadaActual + " no se encuentra en la BBDD."));
                    }
                }
                this.request.setAttribute(OBJETO_JSON, respuesta);
            } else {
                hayError = true;
            }
        } catch (final JSONException ex) {
            hayError = true;
            Logger.getLogger(SincronizarController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (final ParseException ex) {
            hayError = true;
            Logger.getLogger(SincronizarController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (final InterruptedException ex) {
            Logger.getLogger(SincronizarController.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (hayError) {
                try {
                    this.request.setAttribute(OBJETO_JSON, new JSONArray().put(new JSONObject().put(ConstantsJSON.PISTA_KEY_ESTADO, ConstantsJSON.PISTA_KEY_ESTADO_FALLO).put(ConstantsJSON.PISTA_KEY_ESTADO_MENSAJE, "Se deben especificar las pistas junto a sus metadatos (id, nombre, artista, idGenero, fechaModiNombreCancion, fechaModiArtista, fechaModiGenero) a modificar.")));
                } catch (JSONException ex) {
                    Logger.getLogger(SincronizarController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    /**
     * Compara dos pistas para obtener una única pista con los datos más
     * novedosos de ambas. Actualiza la base de datos con dicha pista fusionada.
     */
    private final class ThreadComparador extends Thread {

        Pista pistaActualBBDD;
        Pista pistaModificada;
        Date fechaActual;
        JSONArray respuesta;

        public ThreadComparador(final Pista pistaActualBBDD, final Pista pistaModificada, final Date fechaActual, final JSONArray respuesta) {
            super();
            this.pistaActualBBDD = pistaActualBBDD;
            this.pistaModificada = pistaModificada;
            this.fechaActual = fechaActual;
            this.respuesta = respuesta;
        }

        /**
         * Obtiene la diferencia entre dos fechas (en minutos). Source:
         * http://www.mkyong.com/java/how-to-calculate-date-time-difference-in-java/
         *
         * @param dSuperior
         * @param dInferior
         * @return Diferencia entre las fechas en minutos.
         */
        private long getMiliDiff(final long dSuperior, final long dInferior) {
            return (dSuperior - dInferior);
        }
        private final static String API_UTF_8_KEY = "UTF-8";

        @Override
        public void run() {
            try {
                // Se comparan las fechas de edición de cada una
                final Timestamp pistaModificadaFechaModificacionNombreCancion = pistaModificada.getFechaModificacionNombreCancion();
                final Timestamp pistaModificadaFechaModificacionArtista = pistaModificada.getFechaModificacionArtista();
                final Timestamp pistaModificadaFechaModificacionGenero = pistaModificada.getFechaModificacionGenero();

                // 1. Si alguna es > 60 minutos a la fecha actual: se descartan todas las posteriores operaciones:
                final int MAX_DESFASE_MILISEGUNDOS = 3600000;//3600000=60 minutos
                final long fechaActualLong = fechaActual.getTime();
                final int idPistaActual = pistaActualBBDD.getId();
                if (getMiliDiff(pistaModificadaFechaModificacionNombreCancion.getTime(), fechaActualLong) > MAX_DESFASE_MILISEGUNDOS || getMiliDiff(pistaModificadaFechaModificacionArtista.getTime(), fechaActualLong) > MAX_DESFASE_MILISEGUNDOS || getMiliDiff(pistaModificadaFechaModificacionGenero.getTime(), fechaActualLong) > MAX_DESFASE_MILISEGUNDOS) {
                    System.out.println("\t  - La edición de la pista se rechaza porque alguna fecha es superior a 1 hora de la hora del servidor.");
                    respuesta.put(new JSONObject().put(ConstantsJSON.PISTA_KEY_ID, idPistaActual).put(ConstantsJSON.PISTA_KEY_ESTADO, ConstantsJSON.PISTA_KEY_ESTADO_FALLO).put(ConstantsJSON.PISTA_KEY_ESTADO_MENSAJE, "Se rechaza porque alguna fecha es superior a 1 hora de la hora del servidor."));
                } else {
                    // 2. Todo correcto: existe y no tiene fechas superiores a una hora con elservidor
                    // Se actualiza el valor de las pistas en BBDD en caso de que la modificada sea posterior en tiempo:

                    // 2.1. Se realizan comparaciones
                    boolean hayCambio = false;
                    Timestamp fechaBBDD = pistaActualBBDD.getFechaModificacionNombreCancion();
                    if (fechaBBDD.before(pistaModificadaFechaModificacionNombreCancion)) {
                        System.out.println("\t  - Cambio nombre canción");
                        pistaActualBBDD.setFechaModificacionNombreCancion(pistaModificadaFechaModificacionNombreCancion.after(fechaActual) ? new Timestamp(fechaActualLong) : pistaModificadaFechaModificacionNombreCancion);
                        pistaActualBBDD.setNombreCancion(pistaModificada.getNombreCancion());
                        hayCambio = true;
                    }

                    fechaBBDD = pistaActualBBDD.getFechaModificacionArtista();
                    if (fechaBBDD.before(pistaModificadaFechaModificacionArtista)) {
                        System.out.println("\t  - Cambio artista");
                        pistaActualBBDD.setFechaModificacionArtista(pistaModificadaFechaModificacionArtista.after(fechaActual) ? new Timestamp(fechaActualLong) : pistaModificadaFechaModificacionArtista);
                        pistaActualBBDD.setArtista(pistaModificada.getArtista());
                        hayCambio = true;
                    }


                    fechaBBDD = pistaActualBBDD.getFechaModificacionGenero();
                    if (fechaBBDD.before(pistaModificadaFechaModificacionGenero)) {
                        System.out.println("\t  - Cambio genero");
                        pistaActualBBDD.setFechaModificacionGenero(pistaModificadaFechaModificacionGenero.after(fechaActual) ? new Timestamp(fechaActualLong) : pistaModificadaFechaModificacionGenero);
                        pistaActualBBDD.setIdGenero(pistaModificada.getIdGenero());
                        hayCambio = true;
                    }

                    // 3. Se actualiza en la base de datos si hay algún cambio
                    if (hayCambio) {
                        System.out.println("\t **Gestor de recuperación -> Sincronización BD: prepared.**");
                        System.out.println("\t  - Se actualiza en la base de datos.");
                        pistaActualBBDD = new Persistencia().updatePistaMetadatos(pistaActualBBDD);
                        System.out.println("\t    -> Actualización correcta");
                        System.out.println("\t **Gestor de recuperación -> Sincronización BD: committed.**");

                        // 4. Se propaga a los servidores de réplicas que tengan la pista
                        // 4.1. Me identifico
                        // TODO: Gestor de recuperaciones

                        String auxWeb;
                        JSONObject respuestaJSON;
                        final Map<Integer, Integer> relaciones = pistaActualBBDD.getEstadoRelacionServidor();
                        int estadoActualZona;
                        for (final Map.Entry<Integer, Integer> entry : relaciones.entrySet()) {
                            estadoActualZona = entry.getValue();
                            System.out.println("\t **Gestor de recuperación -> Sincronización Replicación: prepared.**");
                            if (estadoActualZona == ConstantsJSON.FICHERO_EN_SERVIDOR) {
                                auxWeb = zonaToTomcat(entry.getKey()); // esto me da por ejemplo http://pasarela.lab.inf.uva.es:300X3/
                                System.out.println("\t\t  " + auxWeb);
                                respuestaJSON = new JSONObject(HttpConnection.conexionHttp(auxWeb + "Identificacion?" + ConstantsJSON.BACKUP_API_KEY_ID + "=1&" + ConstantsJSON.BACKUP_API_KEY_PASS + "=servidor0", "POST").toString());
                                if (respuestaJSON.optString(ConstantsJSON.BACKUP_API_KEY_ESTADO).equals(ConstantsJSON.BACKUP_API_KEY_ESTADO_EXITO)) {
                                    // 4.2. Si identificación correcta mando replicar
                                    System.out.println("\t\t  --> Identificación correcta, mando actualizar el cambio en las réplicas."); // 3 es el puerto 8080 http para las conexiones http
                                }
                                // TODO si fallo:
                                HttpConnection.conexionHttp(auxWeb + "Edicion?" + ConstantsJSON.BACKUP_API_KEY_TOKEN + '=' + respuestaJSON.optString(ConstantsJSON.BACKUP_API_KEY_TOKEN)
                                        + '&' + ConstantsJSON.PISTA_API_KEY_ID + '=' + idPistaActual
                                        + '&' + ConstantsJSON.BACKUP_API_KEY_PISTA_NOMBRE_CANCION + '=' + URLEncoder.encode(pistaActualBBDD.getNombreCancion(), API_UTF_8_KEY)
                                        + '&' + ConstantsJSON.BACKUP_API_KEY_PISTA_ARTISTA + '=' + URLEncoder.encode(pistaActualBBDD.getArtista(), API_UTF_8_KEY)
                                        + '&' + ConstantsJSON.BACKUP_API_KEY_PISTA_ID_GENERO + '=' + pistaActualBBDD.getIdGenero(), "POST");
                                System.out.println("\t **Gestor de recuperación -> Sincronización Replicación: committed.**");
                            }
                        }
                    }

                    // 5. Se añaden los datos de la nueva pista
                    respuesta.put(new JSONObject().
                            put(ConstantsJSON.BACKUP_API_KEY_ESTADO, ConstantsJSON.BACKUP_API_KEY_ESTADO_EXITO).
                            put(ConstantsJSON.PISTA_KEY_ID, pistaActualBBDD.getId()).
                            put(ConstantsJSON.PISTA_KEY_NOMBRE_CANCION, pistaActualBBDD.getNombreCancion()).
                            put(ConstantsJSON.PISTA_KEY_ARTISTA, pistaActualBBDD.getArtista()).
                            put(ConstantsJSON.PISTA_KEY_ID_GENERO, pistaActualBBDD.getIdGenero()).
                            put(ConstantsJSON.PISTA_KEY_FECHA_MOD_NOMBRE_CANCION, ConstantsJSON.fechasFormatter.format(pistaActualBBDD.getFechaModificacionNombreCancion())).
                            put(ConstantsJSON.PISTA_KEY_FECHA_MOD_ARTISTA, ConstantsJSON.fechasFormatter.format(pistaActualBBDD.getFechaModificacionArtista())).
                            put(ConstantsJSON.PISTA_KEY_FECHA_MOD_GENERO, ConstantsJSON.fechasFormatter.format(pistaActualBBDD.getFechaModificacionGenero())).
                            put(ConstantsJSON.PISTA_KEY_HASH, pistaActualBBDD.getHash()));


                }
            } catch (final Exception ex) {
            }
        }
    }

    private String zonaToTomcat(final int zona) {
        return "http://pasarela.lab.inf.uva.es:300" + (zona == 1 ? 3 : zona == 2 ? 4 : zona == 3 ? 5 : 0) + "3/";
    }
}