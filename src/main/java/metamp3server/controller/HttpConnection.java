package metamp3server.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Establece comunicaicón vía http.
 *
 * @author MetaMP3ASAI1415
 * @since 0.1-ALPHA
 * @version 0.1-ALPHA
 */
public class HttpConnection {

    /**
     * Makes an http connection to get a String response. If any error ocurrs it
     * is not throwed.
     *
     * @param url URL to make the http connection.
     * @param method Standard connection:GET, POST.
     * @return String returned after making the http call.
     */
    public static StringBuilder conexionHttp(final String url, final String method) {
        if (method.compareTo("GET") != 0 && method.compareTo("POST") != 0) {
            throw new IllegalArgumentException("GET or POST method are only valid.");
        }
        final StringBuilder sb = new StringBuilder();
        try {
            final HttpURLConnection c = (HttpURLConnection) new URL(url).openConnection();
            c.setRequestMethod(method);
            c.connect();
            final BufferedReader br = new BufferedReader(new InputStreamReader(
                    c.getInputStream()));
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            br.close();
        } catch (final MalformedURLException ex) {
            ex.printStackTrace();
        } catch (final IOException e) {
            e.printStackTrace();
        }
        return sb;
    }
}