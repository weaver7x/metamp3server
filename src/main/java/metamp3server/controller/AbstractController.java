package metamp3server.controller;

import javax.servlet.http.HttpServletRequest;

/**
 * Abstract Controller that gives a URL web page.
 *
 * @author MetaMP3ASAI1415
 * @since 0.1-ALPHA
 * @version 0.1-ALPHA
 */
public abstract class AbstractController implements IController {

    /**
     * Objeto JSON que erá devuelto en la llamada.
     */
    public final static String OBJETO_JSON = "objetoJSON";
    /**
     * Request object.
     */
    protected HttpServletRequest request;

    /**
     *
     * @param request
     */
    @Override
    public final void init(final HttpServletRequest request) {
        this.request = request;
    }
}