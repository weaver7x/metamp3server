/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package metamp3server.controller;

import java.util.List;
import metamp3server.entity.Pista;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 * Métodos comunes para transformar a objetos JSON
 *
 * @author MetaMP3ASAI1415
 * @since 0.1-ALPHA
 * @version 0.1-ALPHA
 */
public class UtilsJSON {

    /**
     * Transforma una lista de Pistas a formato JSON.
     *
     * @param pistas Lista de pistas.
     * @return JSON Array de pistas.
     */
    static JSONArray pistasToList(final List<Pista> pistas, final boolean muestraURL) {
        JSONArray pistasJSON = new JSONArray();
        JSONObject pistaJSON;

        for (final Pista pista : pistas) {
            try {
                pistaJSON = new JSONObject().
                        put(ConstantsJSON.PISTA_KEY_ID, pista.getId()).
                        put(ConstantsJSON.PISTA_KEY_NOMBRE_CANCION, pista.getNombreCancion()).
                        put(ConstantsJSON.PISTA_KEY_ARTISTA, pista.getArtista()).
                        put(ConstantsJSON.PISTA_KEY_ID_GENERO, pista.getIdGenero()).
                        put(ConstantsJSON.PISTA_KEY_FECHA_MOD_NOMBRE_CANCION, ConstantsJSON.fechasFormatter.format(pista.getFechaModificacionNombreCancion())).
                        put(ConstantsJSON.PISTA_KEY_FECHA_MOD_ARTISTA, ConstantsJSON.fechasFormatter.format(pista.getFechaModificacionArtista())).
                        put(ConstantsJSON.PISTA_KEY_FECHA_MOD_GENERO, ConstantsJSON.fechasFormatter.format(pista.getFechaModificacionGenero())).
                        put(ConstantsJSON.PISTA_KEY_HASH, pista.getHash());
                if (muestraURL) {
                    pistaJSON.put(ConstantsJSON.PISTA_API_KEY_URL, pista.getUrl());
                }
                pistasJSON.put(pistaJSON);
            } catch (final JSONException ex) {
            }
        }
        return pistasJSON;
    }
}