package metamp3server.controller;

import java.text.SimpleDateFormat;

/**
 * Constantes de los protocolos de JSON.
 *
 * @author MetaMP3ASAI1415
 * @since 0.1-ALPHA
 * @version 0.1-ALPHA
 */
public final class ConstantsJSON {

    private ConstantsJSON() {
    }
    /**
     * Formato de las fechas a seguir en el protocolo de la API.
     */
    public final static SimpleDateFormat fechasFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    /**
     * Clave que representa la id de una pista.
     */
    public final static String PISTA_KEY_ID = "id";
    /**
     * Clave que representa el nombre de canción de una pista.
     */
    public final static String PISTA_KEY_NOMBRE_CANCION = "nombreCancion";
    /**
     * Clave que representa el artista de una pista.
     */
    public final static String PISTA_KEY_ARTISTA = "artista";
    /**
     * Clave que representa el genero de canción de una pista.
     */
    public final static String PISTA_KEY_ID_GENERO = "idGenero";
    /**
     * Clave que representa la fecha de modificación del nombre de la canción de
     * una pista.
     */
    public final static String PISTA_KEY_FECHA_MOD_NOMBRE_CANCION = "fechaModiNombreCancion";
    /**
     * Clave que representa la fecha de modificación del artista de la canción
     * de una pista.
     */
    public final static String PISTA_KEY_FECHA_MOD_ARTISTA = "fechaModiArtista";
    /**
     * Clave que representa la fecha de modificación del género de la canción de
     * una pista.
     */
    public final static String PISTA_KEY_FECHA_MOD_GENERO = "fechaModiGenero";
    /**
     * Clave que representa la fecha de modificación del hash de la canción de
     * una pista.
     */
    public final static String PISTA_KEY_HASH = "hash";
    /**
     * Clave que representa la fecha de modificación del id de la canción de una
     * pista.
     */
    public final static String PISTA_API_KEY_ID = "id";
    /**
     * Clave que representa la fecha de modificación de la zona de la canción de
     * una pista.
     */
    public final static String PISTA_API_KEY_ZONA = "zona";
    /**
     * Clave que representa la fecha de modificación de la url de la canción de
     * una pista.
     */
    public final static String PISTA_API_KEY_URL = "url";
    /**
     * Número máximo de descargas permitidas para clientes cuya zona no coincide
     * con la de descarga.
     */
    public final static byte MAXIMO_NUMERO_DESCARGAS_CAMBIO = 4;
    /**
     * Clave que representa el identificador del servidor central-
     */
    public final static String BACKUP_API_KEY_ID = "id";
    /**
     * Clave que representa la contraseña para la identificación.
     */
    public final static String BACKUP_API_KEY_PASS = "pass";
    /**
     * Clave que representa el estado de una peticición.
     */
    public final static String BACKUP_API_KEY_ESTADO = "estado";
    /**
     * Clave que representa el estado de la peticición ha sido exitoso.
     */
    public final static String BACKUP_API_KEY_ESTADO_EXITO = "exito";
    /**
     * Clave que representa el estado de la peticición ha fallado.
     */
    public final static String BACKUP_API_KEY_ESTADO_FALLO = "fallo";
    /**
     * Clave que representa el estado de la peticición ha fallado porque no se
     * estaba logeado.
     */
    public final static String BACKUP_API_KEY_ESTADO_FALLO_NO_LOGIN = "nologin";
    /**
     * Clave que representa el token obtenido tras la identificaicón.
     */
    public final static String BACKUP_API_KEY_TOKEN = "token";
    /**
     * Clave que representa el identificador del nombre de la canción de la
     * pista a editar.
     */
    public final static String BACKUP_API_KEY_PISTA_NOMBRE_CANCION = "nombreCancion";
    /**
     * Clave que representa el identificador del género del artista de la pista
     * a editar.
     */
    public final static String BACKUP_API_KEY_PISTA_ARTISTA = "artista";
    /**
     * Clave que representa el identificador del género de la pista a editar.
     */
    public final static String BACKUP_API_KEY_PISTA_ID_GENERO = "idGenero";
    /**
     * Indica que el fichero se encuentra en el servidor.
     */
    public final static byte FICHERO_EN_SERVIDOR = -1;
    /**
     * Clave que representa el identificador de las pistas para la
     * sincronización.
     */
    public final static String PISTA_KEY_SINCRONIZAR_PISTAS = "pistas";
    /**
     * Clave que representa el estado de una sincronización.
     */
    public final static String PISTA_KEY_ESTADO = "estado";
    /**
     * Clave que representa el estado exitoso de una sincronización.
     */
    public final static String PISTA_KEY_ESTADO_EXITO = "exito";
    /**
     * Clave que representa el estado fallido de una sincronización.
     */
    public final static String PISTA_KEY_ESTADO_FALLO = "fallo";
    /**
     * Clave que representa el estado mensaje de fallo de una sincronización.
     */
    public final static String PISTA_KEY_ESTADO_MENSAJE = "mensaje";
    /**
     * Clave que identifica el directorio WebDAV de los servidores de datos.
     */
    public final static String WEB_DAV = "webdav";
    /**
     * Clave que identifica el puerto de conexión WebDAV de los servidores de
     * datos.
     */
    public final static char WEB_DAV_PUERTO = '3';
}