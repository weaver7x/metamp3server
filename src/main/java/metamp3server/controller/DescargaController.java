package metamp3server.controller;

import java.util.ArrayList;
import java.util.List;
import metamp3server.dao.Persistencia;
import metamp3server.entity.Pista;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 * Controlador del metodo de la API del servidor /Descarga
 *
 * @author MetaMP3ASAI1415
 * @since 0.1-ALPHA
 * @version 1.0-FINAL
 */
public final class DescargaController extends AbstractController {

    @Override
    public void execute() throws ControllerException {
        final String idsJSON = this.request.getParameter(ConstantsJSON.PISTA_API_KEY_ID);
        final String zonaIDJSON = this.request.getParameter(ConstantsJSON.PISTA_API_KEY_ZONA);
        try {
            if (idsJSON != null && zonaIDJSON != null) {

                // 1. Buscar las zonas posibles donde se encuentra los ficheros
                final int zonaID = Integer.parseInt(zonaIDJSON);
                final JSONArray idsPistasJSON = new JSONArray(idsJSON);
                final int numeroPistas = idsPistasJSON.length();
                System.out.println("/Descarga " + numeroPistas + " pistas");
                final List<Integer> idsPistas = new ArrayList<Integer>();
                for (int i = 0; i < numeroPistas; i++) {
                    idsPistas.add(idsPistasJSON.getInt(i));
                }

                // 2. Devolver las pistas encontradas con sus datos (URI de descarga)
                final List<Pista> pistas = new Persistencia().getPistasRelacion(idsPistas, zonaID);
                this.request.setAttribute(OBJETO_JSON, UtilsJSON.pistasToList(pistas, true));

                // 3. Actualizar la base de datos si la zona del cliente es distinta a la del fichero de descarga
                // 4. Replicar fichero en caso de que la petición provenga de un
                // cliente que no sea de la misma zona del servidor de la pista
                // y se hayan alcanzado las ConstantsJSON.MAXIMO_NUMERO_DESCARGAS_CAMBIO descargas
                new GestorReplicacion().replica(pistas, zonaID);
            } else {
                this.request.setAttribute(OBJETO_JSON, new JSONArray().put(new JSONObject().put(ConstantsJSON.PISTA_KEY_ESTADO, ConstantsJSON.PISTA_KEY_ESTADO_FALLO).put(ConstantsJSON.PISTA_KEY_ESTADO_MENSAJE, "Se deben especificar los ids de las pistas a descargar y la zona del cliente.")));
            }
        } catch (final JSONException ex) {
            ex.printStackTrace();
        }
    }
}